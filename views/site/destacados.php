<?php
use yii\widgets\ListView;

?>

<div>
    <h1>Hombres</h1>
</div>

<div>
    <?=    
        ListView::widget([
            "dataProvider"=>$dataProviderHombres,
            "itemView"=>"_destacados",
            "itemOptions" => [
                'class' => 'col-lg-4',
            ],
            "options" => [
                'class' => 'row',
            ],
            'layout'=>"{items}"
                ]);
    ?>
</div>

<div>
    <h1>Mujeres</h1>
</div>

<div>
    <?=    
        ListView::widget([
            "dataProvider"=>$dataProviderMujeres,
            "itemView"=>"_destacados",
            "itemOptions" => [
                'class' => 'col-lg-4',
            ],
            "options" => [
                'class' => 'row',
            ],
            'layout'=>"{items}"
                ]);
    ?>
</div>

<div>
    <h1>Niños</h1>
</div>

<div>
    <?=    
        ListView::widget([
            "dataProvider"=>$dataProviderNinos,
            "itemView"=>"_destacados",
            "itemOptions" => [
                'class' => 'col-lg-4',
            ],
            "options" => [
                'class' => 'row',
            ],
            'layout'=>"{items}"
                ]);
    ?>
</div>